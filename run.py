# -*- coding: utf-8 -*-
# aws s3://aspirant-temp/revo/pdf/* ls

import json, time
from lib import CustomPDF

import boto3

# import uuid
# uuid.uuid4()
# uuid.uuid4().hex


s3 = boto3.client('s3')
# with open("result.pdf", "rb") as f:
#     s3.upload_fileobj(f, 'aspirant-temp', 'result.pdf')

# s3.upload_file(
#     # 'FILE_NAME', 'BUCKET_NAME', 'OBJECT_NAME',
#     'result.pdf', 'aspirant-temp', 'result.pdf', key='revo/pdf/'
# )







# Upload a new file
# data = open('result.pdf', 'rb')
# s3.Bucket('my-bucket').put_object(Key='result.pdf', Body=data)
# s3.Bucket('mybucket').put_object(Key='result.pdf', Body=data)
# s3.Bucket('aspirant-temp').put_object(Key='result.pdf', Body=data)



# Let's use Amazon S3
# client = boto3.client('s3')
# s3.upload_file(
#     'FILE_NAME', 'BUCKET_NAME', 'OBJECT_NAME',
#     ExtraArgs={'Metadata': {'mykey': 'myvalue'}}
# )
# response = client.put_object(
#         Bucket='my-top-level-bucket',
#         Body='',
#         Key='test-folder/'
#         )

# client.invoke(
#         FunctionName="loadSpotsAroundPoint",
#         InvocationType='Function',
#         # Payload=payload3
#     )


# s3.Object('aspirant-temp', 'result.pdf').put(Body=open('result.pdf', 'rb'))
# data = open('result.pdf', 'rb')
# s3.Bucket('mybucket').put_object(Key='result.pdf', Body=data)
# for bucket in s3.buckets.all():
#     print(bucket.name)


import boto3
from botocore.client import Config


# s3 = boto3.client('s3', 'us-east-1', config=Config(s3={"addressing_style": "path"}, signature_version="s3v4"))
# # print(s3)
# s3.upload_file('result.pdf', "aspirant-temp", 'result.pdf')


 # pylambda run run.py -e event.json -n handler


def create_pdf(pdf_path, numerate=False, users=False):
    if not users:
        with open('list.json', 'r') as f:
            jfile = json.load(f)
            users = jfile['profiles']

    pdf = CustomPDF()
    pdf.add_font('HelveticaNeue', '', 'fpdf/fonts/HelveticaNeue.ttf', uni=True)
    pdf.add_font('HelveticaNeueLight', '', 'fpdf/fonts/HelveticaNeue Light.ttf', uni=True)
    pdf.add_font('HelveticaNeueThin', '', 'fpdf/fonts/HelveticaNeue Thin.ttf', uni=True)
    pdf.add_font('HelveticaNeueBold', '', 'fpdf/fonts/Helvetica Neu Bold.ttf', uni=True)
    pdf.add_font('HelveticaNeueMedium', '', 'fpdf/fonts/HelveticaNeue Medium.ttf', uni=True)
    pdf.add_font('LucidaGrande', '', 'fpdf/fonts/LucidaGrande.ttf', uni=True)
    pdf.add_font('LucidaGrandeBold', '', 'fpdf/fonts/Lucida Grande Bold.ttf', uni=True)
    pdf.add_font('GillSans', '', 'fpdf/fonts/GillSans Italic.ttf', uni=True)


    pdf.alias_nb_pages()

    pdf.add_page()

    if numerate:
        pdf.image('home.png', 0, 0, 210, 297)

    pdf.set_font('HelveticaNeueLight', '', 28)
    pdf.ln(111)

    with open('list.json', 'r') as f:
        title_file = json.load(f)['listName']

    pdf.cell(0, 0, txt=title_file, ln=0)
    # pdf.cell(10, 25, ln=1)
    pdf.set_font('HelveticaNeueLight', '', 16)
    pdf.ln(25)
    from datetime import datetime
    # +'13th May 2019'
    pdf.cell(0, 0, txt=str(datetime.now().strftime('%d %b %Y')), ln=1)


    pdf.set_font('HelveticaNeueLight', '', 12)
    pdf.header_vis = True
    pdf.add_page()
    pdf.footer_vis = True


    user_id = 1
    for user in users:
        users[user_id-1]['id'] = user_id
        # pdf.set_font('HelveticaNeue', '', 12)
        # pdf.cell(100, 15, user['name'], align='L', ln=0)
        # pdf.set_font('HelveticaNeue', '', 12)
        # pdf.cell(5, 15, ' | ' + str(user['jobTitle']), align='L', ln=1)

        txt = " | {} | {}".format(user['jobTitle'] + ' at ' + user['firm'], user['location'])
        pdf.set_font('HelveticaNeueBold', '', 12)
        pdf.cell(0, 7, user['name'], align='L', ln=0)
        pdf.set_x(pdf.get_string_width(user['name'])+20)
        pdf.set_font('HelveticaNeueLight', '', 12)
        pdf.multi_cell(150, 7, txt, align='J', fill=0)
        # print()

        # pdf.cell(0, 15, txt=txt, border='B', align='L', ln=0)
        pdf.set_font('HelveticaNeueLight', '', 12)
        pdf.set_x(150)
        if numerate:
            try:
                if user['num_range']:
                    pdf.multi_cell(40, -7, txt=str(user['num_range']), align='R')  # number of page
                else:
                    pdf.set_x(147)
                    pdf.multi_cell(40, -7, txt=str(user['num_page']), align='R')  # number of page
            except:
                pdf.set_x(147)
                pdf.multi_cell(40, -7, txt=str(user['num_page']), align='R')  # number of page
        else:
            pdf.multi_cell(40, -7, txt='uid_'+str(user_id)+'_', align='R')  # number of page

        pdf.set_font('HelveticaNeueLight', '', 12)

        pdf.ln(10)
        pdf.set_draw_color(192, 192, 192)
        pdf.cell(170, 0, '', border='B', ln=1)
        pdf.set_draw_color(0)
        # pdf.cell(10, 15, ln=1)
        pdf.ln(5)
        user_id += 1

    # user_id = 1
    # print(len(users))
    for user in users:
        pdf.add_page()
        users[int(user['id'])-1]['num_page'] = pdf.page_no()
        users[int(user['id'])-1]['num_range'] = False

        if not numerate:
            if user['id'] == 1:
                # print('here')
                pass
            elif user['id'] == len(users):
                current = users[int(user['id'])-1]['num_page']
                before = users[int(user['id'])-2]['num_page']
                diff_page = current - before

                # print('last', user['id'])
                if diff_page >= 2:  # if current - before
                    users[int(user['id'])-2]['num_range'] = str(before) + '-' + str(current-1)
                # print(current, len(pdf.pages))
                # print(pdf.pages[20])
                if current - len(pdf.pages)+1 >= 1:
                    users[int(user['id'])-1]['num_range'] = str(current) + '-' + str(len(pdf.pages)+1)
            else:
                current = users[int(user['id'])-1]['num_page']
                before = users[int(user['id'])-2]['num_page']
                diff_page = current - before
                # print(users[int(user['id'])-1]['num_page'], users[int(user['id'])-2]['num_page'], diff_page)
                if diff_page >= 2:  # if current - before
                   users[int(user['id'])-2]['num_range'] = str(before) + '-' + str(current-1)

            # print(user['id'], user['num_page'])
        # try:
        #     if users[user_id-1]['num_page'] - users[user_id-2]['nump_page']



        if not numerate:
            import urllib.request
            urllib.request.urlretrieve(user['image'], '{}.png'.format(user['id']))
            time.sleep(1)  # for AWS S3

        pdf.image('{}.png'.format(user['id']), 20, 26, 28, 28, link=user['image'])

        pdf.set_font('HelveticaNeueBold', '', 18)
        pdf.set_x(51)
        pdf.cell(0, 15, user['name'], ln=1)
        # pdf.text(45, 60, user['name'])

        pdf.set_font('HelveticaNeueLight', '', 14)
        pdf.set_xy(51, 38)
        pdf.cell(0, 15, user['jobTitle'] + ' at ' + user['firm'] + ' | ' + user['location'], ln=1)
        # pdf.text(45, 70, user['jobTitle'] + ' at ' + user['firm'] + ' | ' + user['location'])

        pdf.set_font('HelveticaNeueLight', '', 12)
        pdf.image('ed.jpg', 20, 57, 7, 7)
        pdf.set_xy(28, 58)
        pdf.cell(0, 5, 'Year of first admission: {} ({} years)'.format(user['firstAdmissionDate'],
                                                                         user['yearsSinceFirstAdmission']), ln=1)
        # pdf.text(10, 90, 'Year of first admission: {} ({} years)'.format(user['firstAdmissionDate'],
        #                                                                  user['yearsSinceFirstAdmission']))

        try:
            if user['profileUrl']:
                # pdf.set_font('Times', 'U', 10)
                pdf.set_font('HelveticaNeueLight', 'U', 10)
                pdf.set_text_color(0, 0, 255)
                pdf.image('firm.jpg', 20, 65, 7, 7)
                pdf.set_xy(28, 66)
                pdf.write(5, 'Link to Firm Profile', user['profileUrl'])
                pdf.ln(11)
                profile = True
        except:
            profile = False

        try:
            if user['linkedin']:
                # pdf.set_font('Times', 'U', 10)
                pdf.set_font('HelveticaNeueLight', 'U', 10)
                pdf.set_text_color(0, 0, 255)
                if profile:
                    pdf.image('linkedin.jpg', 20, 73, 7, 7)
                    pdf.set_xy(28, 74)
                else:
                    pdf.image('linkedin.jpg', 20, 66, 7, 7)
                    pdf.set_xy(28, 67)
                pdf.write(5, user['linkedin'], user['linkedin'])

                pdf.set_font('HelveticaNeue', '', 14)
                pdf.set_text_color(0)
                pdf.ln(11)
        except:
            pass

        pdf.set_font('HelveticaNeueMedium', '', 14)
        pdf.set_text_color(0)
        pdf.set_x(20)
        pdf.cell(0, 5, 'Summary', ln=1)
        pdf.ln(2)
        pdf.set_draw_color(55, 196, 183)
        # pdf.cell(170, 0, '', border='B', ln=1)
        pdf.set_line_width(0.37)
        pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())
        pdf.set_draw_color(0)

        pdf.set_font('HelveticaNeueLight', '', 12)
        pdf.ln(2)

        # if user['name'] == 'Amanda Dawe':
        #     print(user['overview'])

        summary_tmp = user['overview'].replace('[!', '[').replace('   ', '').replace('  ', '').replace('##', '').replace('_', '').replace('**', '').replace('* ', '• ')
        summary = ''
        reset = False
        for letter in summary_tmp:
            if letter == '[!' or letter == '[':
                continue
            elif letter == ']':
                continue
            elif letter == '(':
                reset = True
                continue
            elif letter == ')':
                reset = False
                continue
            elif reset:
                continue
            else:
                summary += letter

        # if user['name'] == 'Amanda Dawe':
        summary_text = {}
        i = 0
        dots = False
        # print(len(summary))
        tmp_text = ''
        dots = False
        for letter in summary:
            if letter == '£':
                letter = '(pound sign) '

            if i not in summary_text.keys():
                summary_text[i] = {}
                summary_text[i]['text'] = ''
                summary_text[i]['type'] = ''

            # print(letter)
            if not dots:
                if letter != '•':
                    summary_text[i]['text'] += letter

                    if dots:
                        summary_text[i]['type'] = 'dots'
                    else:
                        summary_text[i]['type'] = 'text'
                else:
                    i += 1
                    dots = True
            else:
                if letter == '\n':
                    i += 1
                    dots = False
                else:
                    summary_text[i]['text'] += letter

                    if dots:
                        summary_text[i]['type'] = 'dots'
                    else:
                        summary_text[i]['type'] = 'text'

            # try:
            #     summary_text[t]
            # except:
            #     summary_text.append('')
            #
            # try:
            #     summary_dots[d]
            # except:
            #     summary_dots.append('')
            #

        # for text in summary_text:
        #     # print(text)
        #     pdf.multi_cell(170, 5, text, align='J', fill=0)
        for i in summary_text:
            # print(summary_text[i])
            if summary_text[i]['type'] == 'text':
                pdf.multi_cell(170, 6, str(summary_text[i]['text']).replace('•', '').replace('\\', ''), align='J', fill=0)
                pdf.ln(1)
            # elif summary_text[i]['type'] == '':
            #     pdf.ln(5)
            elif summary_text[i]['type'] == 'dots':
                pdf.set_x(19)
                pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                # pdf.set_x(pdf.get_string_width('•')+22)
                pdf.set_x(19 + pdf.get_string_width('• '))
                pdf.multi_cell(170 - pdf.get_string_width('•'), 6, u"{}".format(str(summary_text[i]['text'][1::]).replace('\t', '').replace('•', '')), align='L')
                # print(summary_text[i]['text'])
        # for ranking in user['rankings']:
        #     pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
        #     pdf.multi_cell(170, 6, str(ranking['firm']) + ' | ' + str(ranking['category']) + ' | ' + str(
        #         ranking['practice_chambers']), align='J', fill=0)

        # pdf.multi_cell(170, 5, summary, align='J', fill=0)


        try:
            if user['expertise']:
                users[user['id'] - 1]['is_expertise'] = True
                pdf.ln(10)
                new_y = pdf.get_y()
                pdf.set_font('HelveticaNeueMedium', '', 14)
                pdf.cell(0, 5, 'Expertise', ln=1)
                pdf.ln(1)
                pdf.set_draw_color(55, 196, 183)
                # pdf.cell(170, 0, '', border='B', ln=1)

                pdf.set_line_width(0.37)

                if numerate:
                    print(users[user['id'] - 1]['is_education'])
                    if users[user['id'] - 1]['is_education']:
                        pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+80, pdf.get_y())
                    else:
                        pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())
                else:
                    pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+80, pdf.get_y())

                pdf.set_draw_color(0)

                pdf.set_font('HelveticaNeueLight', '', 12)
                pdf.ln(1)

                for expertise in user['expertise']:
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(80, 6, str(expertise).replace('   ', '').replace('  ', ''), align='L')

                expertise_end_y = pdf.get_y()
            else:
                expertise_end_y = 0
                users[user['id'] - 1]['is_expertise'] = False
        except:
            user['expertise'] = False
            expertise_end_y = 0
            users[user['id'] - 1]['is_expertise'] = False

        try:
            if user['education']:
                users[user['id'] - 1]['is_education'] = True
                # pdf.ln(5)
                pdf.set_font('HelveticaNeueMedium', '', 14)
                pdf.set_xy(109, new_y)
                pdf.cell(0, 5, 'Education', ln=1)
                pdf.ln(1)
                pdf.set_draw_color(55, 196, 183)
                # pdf.cell(170, 0, '', border='B', ln=1)
                pdf.set_line_width(0.37)
                if numerate:
                    if users[user['id'] - 1]['is_expertise']:
                        pdf.line(pdf.get_x()+90, pdf.get_y(), pdf.get_x()+170, pdf.get_y())
                    else:
                        pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())
                else:
                    pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())
                pdf.set_draw_color(0)

                pdf.set_font('HelveticaNeueLight', '', 12)
                pdf.ln(1)

                for education in user['education']:
                    pdf.set_x(109)
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(80, 6, (str(education)).replace('   ', '').replace('  ', ''), align='L')

                education_end_y = pdf.get_y()
            else:
                education_end_y = 0
                users[user['id'] - 1]['is_education'] = False
        except:
            education_end_y = 0
            users[user['id'] - 1]['is_education'] = False

        try:
            if user['clientWork']:
                # maximum = 0
                # if expertise_end_y > education_end_y:
                #     maximum = expertise_end_y
                # elif expertise_end_y < education_end_y:
                #     maximum = education_end_y
                # print(maximum)
                # pdf.set_xy(19, maximum)
                pdf.set_xy(19, max(expertise_end_y, education_end_y))
                pdf.ln(10)
                pdf.set_font('HelveticaNeueMedium', '', 14)
                pdf.cell(0, 5, 'Past Client Work', ln=1)
                pdf.ln(2)
                pdf.set_draw_color(55, 196, 183)
                # pdf.cell(170, 0, '', border='B', ln=1)
                pdf.set_line_width(0.37)
                pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())

                pdf.set_draw_color(0)

                pdf.ln(2)
                pdf.set_font('HelveticaNeueLight', '', 12)
                client_work = str(user['clientWork']).replace('\t', ' ').replace('   ', '').replace('  ', '').replace('*', '')

                client_work = client_work.split('\n')
                # if user['id'] == 12:
                    # print(client_work)

                for client in client_work:
                    client = (str(client)).replace('\n', '').replace('     ', '').replace('    ', '').replace('   ', '').replace('  ', '')
                    if client != '':
                        pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                        # print(client)
                        if client[0] == ' ':
                            client = client[1::]
                        pdf.multi_cell(170 - pdf.get_string_width('•'), 6, client, align='L')
                    else:
                        continue

                # if client_work[0] == '•':
                #     pdf.multi_cell(170, 6, client_work, align='L', fill=0)
                # else:
                #     pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                #     pdf.multi_cell(170, 6, client_work, align='L', fill=0)
                pdf.ln(1)
        except:
            pass

        try:
            if user['rankings']:
                pdf.ln(10)
                pdf.set_font('HelveticaNeueMedium', '', 14)
                pdf.cell(0, 5, 'Chambers & Partners Rankings', ln=1)
                pdf.ln(2)
                pdf.set_draw_color(55, 196, 183)
                # pdf.cell(170, 0, '', border='B', ln=1)
                pdf.set_line_width(0.37)
                pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x()+170, pdf.get_y())
                pdf.set_draw_color(0)

                pdf.ln(2)
                pdf.set_font('HelveticaNeueLight', '', 12)

                for ranking in user['rankings']:
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(170 - pdf.get_string_width('•'), 6, str(ranking['firm']), align='J', fill=0)
            # + ' | ' + str(ranking['category']) + ' | ' + str(ranking['practice_chambers'])
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(170 - pdf.get_string_width('•'), 6, str(ranking['rank']), align='J', fill=0)
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(170 - pdf.get_string_width('•'), 6, str(ranking['practice_chambers']), align='J', fill=0)
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)

                    comment_new = ranking['comment'].replace('\t', ' ').replace('     ', '').replace('    ', '').replace('   ', '').replace('  ', '')

                    if comment_new[0] == ' ':
                        comment_new = comment_new[1::]

                    pdf.multi_cell(170 - pdf.get_string_width('•'), 6, str(comment_new), align='J', fill=0)
                    pdf.cell(pdf.get_string_width('• '), 6, '•', ln=0)
                    pdf.multi_cell(170 - pdf.get_string_width('•'), 6, str(ranking['guide']), align='J', fill=0)

                    pdf.ln(4)
                    pdf.set_draw_color(55, 196, 183)
                    # pdf.cell(170, 0, '', border='B', ln=1)
                    pdf.set_line_width(0.2)
                    pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x() + 170, pdf.get_y())
                    pdf.set_draw_color(0)
                    pdf.ln(7)
                    # "firm": "Skadden, Arps, Slate, Meagher & Flom (UK) LLP",
                    # "category": "International Arbitration: Commercial Arbitration - UK-wide",
                    # "rank": "Band 4",
                    # "practice_chambers": "Dispute Resolution & Litigation - International Arbitration",
                    # "comment": "Clients report that  Daniel Gal is 'technically and strategically excellent, and an impressive advocate.' He has particular expertise in infrastructure disputes and matters involving Middle Eastern disputes.",
                    # "guide": "UK Guide, 2019"
        except:
            pass

        # user_id += 1

    if not numerate:
        create_pdf(pdf_path, numerate=True, users=users)
    else:
        pdf.output(pdf_path)


def handler(event, context):
    event["json_url"]  # "s3://mybucket/myjsonfile.json",
    event["pdf_url_prefix"]  # "s3://mybucket/myoutputfolder/"

    # body of function with reate_pdf('result.pdf')

    return event["pdf_url_prefix"]

# if __name__ == '__main__':
#     create_pdf('result.pdf')




import json
def hello(event, context):
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
    # Use this code if you don't use the http event with the LAMBDA-PROXY
    # integration
    """
    return {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "event": event
    }
    """
