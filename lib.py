from __future__ import division

import os, sys, zlib, struct, re, tempfile, struct

from fpdf.php import substr, sprintf, print_r, UTF8ToUTF16BE, UTF8StringToArray
from fpdf.py3k import PY3K, pickle, urlopen, Image, basestring, unicode, exception, b, hashpath

from fpdf.fpdf import FPDF
import json





class CustomPDF(FPDF):
    header_vis=False
    footer_vis=False

    def __init__(self, orientation='P', unit='mm', format='A4'):
        super(CustomPDF, self).__init__()

        self.set_margins(19, 8, 20)
        # self.set_font('times', 'B', 15)

    def header(self):
        if self.header_vis:
            with open('list.json', 'r') as f:
                title_file = json.load(f)['listName']

            self.set_font('HelveticaNeueThin', '', 12)
            self.set_text_color(92, 92, 92)
            self.set_draw_color(192, 192, 192)
            self.cell(0, 15, title_file, border='B', ln=0, align='R')
            self.set_draw_color(0)
            self.ln(20)
            # self.cell(160, border='B', ln=1)  # w,h=0,txt='',border=0,ln=0,align='',fill=0,link=''


    def footer(self):
        if self.footer_vis:
            self.set_line_width(0)
            self.set_y(-20)

            self.set_font('HelveticaNeueLight', '', 10)
            self.set_text_color(92, 92, 92)
            self.set_draw_color(192, 192, 192)

            self.cell(170, 5, border='B', ln=1)
            self.set_draw_color(0)

            self.image('logo.png', 19, 284, 33)
            page = str(self.page_no()) + ' of {nb}'
            self.cell(172, 11, page, 0, 0, align='R')